+++
title = "Sobre enseñanza online"
date = 2020-03-27
[taxonomies]
tags = ["teaching"]
+++

Estos días nos encontrados encerrados en nuestras casas por el coronavirus
(COVID-19), por lo que en mi universidad (en  Granada), como en el resto de
universidades de España, tuvimos que convertirnos en _expertos en enseñanza
online_ en un fín de semana.
<!-- more -->

Para las asignaturas tuvimos distintos enfoques: Material escrito adicional,
varios vídeos cortos sobre el contenido, o charlas para dar el curso mediante
videoconferencias. Todas las opciones tienen sus ventajas y desventajas. Sin
embargo, como me gusta el _feedback_ con los alumnos (por ejemplo hacer varios
ejemplos en código sobre los conceptos de mi curso de programación), finalmente
he decidido dar el curso por videoconferencia. Para recomendación de mi
universidad utilizo [google meet](https://meet.google.com/), utilizado también
por los otros profesores del curso. Personalmente, prefiero
[Jitsi](https://meet.jit.si/), no sólo es de código abierto sino que también es
comparable en características a la opción de google, es una gran opción.

Una parte de la enseñanza por videoconferencia, pongo todas las diapositivas en el
Moodle de los cursos (en realidad, ya estaba allí), y trato de poner
algún cuestionario para que los alumnos tengan un mejor feedback sobre sus conocimientos.

El principal problema ha sido tener un alumno sordo. La videoconferencia no es
no es buena, y he pasado varios días probando varias herramientas para
subtitular automáticamente la voz, pero en español demasiadas herramientas no
dan un buen rendimiento (tienen tienen muchos errores), finalmente la mejor
opción fue [Ability Connect](https://abilityconnect.ua.es/?lang=en), una
herramienta de la Universidad de Alicante (España) para ello. Por desgracia, a
veces se queda pillada.

Algunos deben pensar que tendré menos trabajo, pero en realidad la situación
me ha hecho trabajar más en la enseñanza. Es bueno aprender nuevas habilidades, pero lleva
mucho tiempo.
