+++
title = "Charla en JuliaCon2022"
date = 2022-07-27
[taxonomies]
tags = ["teaching", "julia"]
+++

Hoy se verá mi charla en el JuliaCon 2022, sobre mis experiencias docentes
usando Julia.

Se puede ver aquí:
[https://live.juliacon.org/talk/KGLNUH](https://live.juliacon.org/talk/KGLNUH)

Después de la hora de la charla, está libremente disponible en Youtube.

El código fuente de las demos está disponible en:
[https://gitlab.com/daniel.molina/mh_demos/](https://gitlab.com/daniel.molina/mh_demos/)
