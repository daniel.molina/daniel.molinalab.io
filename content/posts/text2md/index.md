+++
title = "Creando notebooks desde Markdown"
date = 2022-11-03
[taxonomies]
tags = ["emacs", "python", "markdown", "teaching"]
+++

El uso de Notebooks en Python es muy útil, especialmente para enseñanza.
Sin embargo, aunque el interfaz web (y el uso de
[EIN](https://tkf.github.io/emacs-ipython-notebook/) desde Emacs), siempre me
resulta un poco "latazo" tener que moverme entre celdas, intercalando texto en
`Markdown` y en `Python` (o `Julia`).

Afortunadamente, existen alternativas. La más genérica y que me gusta más se
llama [jupytext](https://jupytext.readthedocs.io/en/latest/) y permite convertir
un fichero markdown en un notebook.

La sintaxis es sencilla, el texto en claro pasará a ser celdas markdown, y el
código en Python pasará a ser celdas de código.

Por ejemplo:

<pre>
### Creando DataFrames desde cero

Se puede crear a partir de un simple `diccionario`.

En el ejemplo tenemos un puesto de frutas que vende manzanas y naranjas. Queremos una columna por cada fruta y una fila por cada compra de un cliente.


```python
data = {
    'apples': [3, 2, 0, 1],
    'oranges': [0, 3, 7, 2],
    'propina': ['si', 'no', 'si', 'si']
}
```

```python
purchases = pd.DataFrame(data)

purchases
```

El **Indice** de este DataFrame se creó automaticamente al iniciarlo, usando los números0-3, pero podemos asignar los que queramos.
</pre>

Quedaría con dos celdas markdown al principio y final, y dos celdas de código en
medio:

![Ejemplo de Salida](notebook.png)

Para compilar los distintos ficheros markdown a notebook uso los siguientes
ficheros.

- Un fichero **makefile** que compila automáticamente todos los ficheros md a notebook.

```sh
files:=$(wildcard *.md) # 1 line
OBJ:=$(files:.md=.ipynb) # 2 line

all: $(OBJ)

%.ipynb: %.md
	jupytext $< --update --to notebook # update es para mantener los IDs en el fichero.

clean:
	rm *.ipynb

```

- Un pequeño script **run.sh** que permite ejecutarlo cuando un fichero cambia
  gracias al excelente programa
  [watchexec](https://lib.rs/crates/watchexec-cli), aunque otros programas como
  [entr](https://github.com/eradman/entr) también valdría:

```sh
watchexec -e md make
```

De esta manera, cuando cualquier fichero con extensión markdown (ignora el
resto) es modificado, automáticamente llama a make, que cambiará el notebook
correspondiente.
