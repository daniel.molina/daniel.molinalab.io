+++
title = "Listado como uno de los investigadores más influyentes"
date = 2023-10-18
[taxonomies]
tags = ["research"]
+++

Ha salido mi nombre en la lista de los investigadores más influyentes del mundo, según el ranking de Stanford, que considera aquellos investigadores de todas las áreas que más influencia (citas) reciban:

[https://www.granadahoy.com/granada/Universidad-Granada-investigadores-influyentes-Stanford_0_1838516842.html](https://www.granadahoy.com/granada/Universidad-Granada-investigadores-influyentes-Stanford_0_1838516842.html)

[https://www.ugr.es/universidad/noticias/ugr-record-154-investigadores-mas-influyentes-mundo](https://www.ugr.es/universidad/noticias/ugr-record-154-investigadores-mas-influyentes-mundo)

Y estaba en ese elemento 114538, que puede parecer poco, pero teniendo en cuenta que el 2% de los más influyentes llega hasta 209602 nombres, es claro que es un mérito importante.

Es más, solo 154 científicos de la UGR están en ese listado, de todas las áreas, y estoy posicionado cerca de la mitad de la lista según la lista ordenada de Stanford, en mejor posición  que investigadores con más experiencia y catedráticos reconocidos, lo cual es todo un logro 👍.
