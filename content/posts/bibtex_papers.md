+++
title = "Añadiendo referencias con Zola y Bibtex"
date = 2023-08-17
[taxonomies]
tags = ["research"]
+++

Hace mucho que no publico en el Blog por falta de tiempo (y de ganas en vacaciones).

Pero acabo de retocarlo para añadir las publicaciones, y como es algo que por mucho que he buscado en Internet no he encontrado nada, lo publico aquí. 

A continuación la guía de cómo listar automáticamente publicaciones usando Bibtex y Zola.

Lo primero ha sido crear un fichero .bib con todas mis publicaciones. Por suerte ha sido lo más fácil, he usado [Scopus](https://www.scopus.com/) para ello.

Una vez copiado en un directorio /data/ he creado una página con una plantilla nueva, *paper_lists.html*.

El grueso de dicha página es la instrucción siguiente en la cabecera:

```
template = "papers_list.html"
```

A continuación vamos a ver el fichero `papers_list.html`:

Primero cargo el fichero en la variable _data_:

```html
{% set data = load_data(path="/data/all.bib", format="bibtex") -%}
```

En `data.bibliographies` tengo un vector de publicaciones. Aparte del atributo `entry_type` que indica el tipo de publicación (yo filtro por "ARTICLE") está el atributo `tags` que contiene los típicos atributos de un paper: title, author, journal, year, volume, pages, ...

Primero hago un filtrado por los artículos ordenados por año:

```html
{% set sorted_bib = data.bibliographies | filter(attribute="entry_type", value="ARTICLE") %}
{% for bib in sorted_bib | sort(attribute="tags.year") | reverse %}
...
{% endfor %}
```

Dado que quiero mostrarlo por año, quiero mostrar sólo el año si cambia respecto al anterior. Para ello uso una variable global `last_year`:

```html
{% if last_year != bib.tags.year %}
	    <h1 class="title">{{bib.tags.year}}</h1>
	    {% set_global last_year = bib.tags.year %}
{% endif %}
```

El resto es sencillo, es simplemente utilizar `bib.tags`. He intentado usar macros, pero tiene limitaciones. 

A continuación muestro el código en completo.

```html
{% set data = load_data(path="/data/all.bib", format="bibtex") -%}

{% if data %}
<section class="section">
    <div class="container">
	<div class="columns is-centered">
	    {% set_global last_year="2000" %}
	<div class="column is-9">
	    <h2 class="title">{{trans(key="papers",lang=lang)}}</h2>
	    {% set sorted_bib = data.bibliographies | filter(attribute="entry_type", value="ARTICLE") %}
	    {% for bib in sorted_bib | sort(attribute="tags.year") | reverse %}
	    {% if last_year != bib.tags.year %}
	    <h1 class="title">{{bib.tags.year}}</h1>
	       {% set_global last_year = bib.tags.year %}
	    {% endif %}
	    {# __tera_context#}
	    <article class="box">
            <h2>
		<b>
		{% if bib.tags.doi %}
		<a class="has-text-dark" href='http://dx.doi.org/{{bib.tags.doi}}' target="_blank">
            {{ bib.tags.title }}
		</a>
		{% else %}
		{{ bib.tags.title }}
		{% endif %}
		</b>{{ bib.tags.author}}. {{ bib.tags.journal}}. vol: {{bib.tags.volume}}, {% if bib.tags.pages %}pages: {{bib.tags.pages}}{% else %} In Press {% endif %} ({{bib.tags.year}}).
		{#
		{% if bib.tags.note != "cited By 0" %}
		{{bib.tags.note}}
		{% else %}
		{{bib.tags.note}}
		{% endif %}
		#}
		{% if bib.tags.note and bib.tags.note != "Cited By 0" %}
		{% set citations = bib.tags.note | split(pat=" ") | last | int %}
		{% if citations >= 5  %}
		<b>{{trans(key="citations", lang=lang)}}: {{ citations }}</b>
		{% endif %}
		{% endif %}
	    </h2>
	</article>
	  {% endfor %}
      </div>
    </div>
</section>
{% endif %}
```
