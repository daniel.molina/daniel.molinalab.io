+++
title = "Un nuevo inicio"
date = 2022-06-28
+++

Sé que tengo el blog muy parado, y creo saber por qué. El motivo es el idioma.

No es que no sepa inglés, la verdad es que lo domino cada vez más, pero es algo
que me echa para atrás a la hora de escribir en mis propio tiempo libre. Por
tanto, he decidido empezar a escribir en español/andaluz/granaíno. De esa forma
espero animarme a escribir más, porque tengo temas para hacerlo y nunca lo acabo
haciendo.

Así pues, aquí empieza mi nuevo inicio.

<!-- more -->
