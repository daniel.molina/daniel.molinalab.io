+++
title= "Miro el móvil"
date = 2022-07-09
[taxonomies]
tags = ["personal", "poesia"]
+++

Miro el móvil\
solo para ver el fondo de pantalla\
para ver tu cara\
tu sonrisa de un tiempo que se fue.\

Miro el móvil\
esperando tus mensajes,\
mensajes que no llegan\
porque te perdí.\

Miro el móvil
porque no me acompañas,\
porque te has ido,\
y el pasado que atesora\
me acompaña en mi soledad.
