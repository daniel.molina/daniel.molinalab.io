+++
title = "Elfeed: Usando emacs para leer RSS"
date = 2017-10-12
[taxonomies]
tags = ["emacs"]
+++

En los últimos años he usado Emacs para casi todas mis tareas diarias:

-   Leyendo mis emails (con [mu4e](http://www.djcbsoftware.nl/code/mu/mu4e.html)).
-   Creando las transparencias de mis cursos usando org-beamer o pandoc.
-   Usando dired para navegar por el sistema de ficheros.
-   Publicar este blog. La nueva versión usa [zola](https:/getzola.org/) y es
    libremente accesible [codeberg](http://codeberg.org/dmolina/personal_page).

Lo último en integrar en emacs es leer blogs y noticias de fuentes RSS.
Añadir [elfeed](https://github.com/skeeto/elfeed) y
[elfeed-org](https://github.com/remyhonig/elfeed-org) fui capaz de crear RSS. elfeed-org
es muy simple, y permite añadir las fuentes como item en org-mode:

```text
- Blogs                                                              :elfeed:

  - https://www.meneame.net/rss                                  :news:portada:
  - https://www.meneame.net/rss?status=queued                            :news:
  - http://planet.emacsen.org/atom.xml                                :emacs:
  - https://www.reddit.com/r/programming/.rss                     :programming:
  ...
```

Las etiquetas para cada fuente serán compartidas por todos los artículos.

Luego, cargando **elfeed**, puede ser obtenido una pantalla mostrando los
distintos artículos.

{{fig(fname="elfeed.png", caption="Prueba de elfeed")}}

Y seleccionando el artículo, puede ser abierto, leído y abierto usando el
navegador por defecto.

{{fig(fname="elfeed2.png", caption="Lectura de artículo con elfeed")}}

Mis opiniones sobre elfeed:

- Muy simple de usar.

- El uso de etiquetas es muy poderoso, no solo reciben las etiquetas de la
  categoría, puedes añadir las que quieras.

- El filtro de búsqueda es simple y potente, puedes filtrar tanto por fechas
  como por etiquetasa.

- El filtro de búsqueda se puede guardar en el marcapáginas (_bookmark_), usando
  C-x r b puede verse los artículos usando un filtro específico.

En resumen, **elfeed** ha sido un gran descubrimiento. Si usas emacs, dale un
intento.
