+++
title = "Configurando Emacs desde cero"
date = 2023-09-05
[taxonomies]
tags = ["emacs"]
+++

Estaba este verano con unos pocos días libres, y pensando en un curso sobre Emacs, y tras la versión 29.1. decidí hacer una configuración por defecto. Bueno, de cero de cero... ya he estado usando spacemacs desde hace años, y ya conozco paquetes que suelo usar, así que no partía de cero.

El proceso ha sido mucho más sencillo de lo esperado, a partir de un fichero .org siguiendo los consejos de https://github.com/frap/emacs-literate.

La configuración está disponible y explicada en [/emacs/](/emacs), generado también a partir del fichero .org (usando babel genero tanto el fichero init.el como la documentación). Lo bueno es que es fácil poner como comentarios, ..., y adaptarlo usando un control de versiones.

Lo he dividido en:

- Configuración general, sin paquetes. 
  a) Configurar la carga de paquetes.
  b) Quitar la página inicial, scroll inicial.
  c) Activar por defecto la papelera.
  d) Pequeños cambios: Hora, quitar algunos mensajes de confirmación, numerar las líneas, ...
  
- Paquetes que considero imprescindibles:

	a) Mostrar la letras, usando [which-key](https://github.com/justbur/emacs-which-key). 
	b) Mejorar el undo con [undo-tree](https://elpa.gnu.org/packages/undo-tree.html)
	c) Ficheros recientes [recentf](https://www.emacswiki.org/emacs/RecentFiles).
	d) Marcar fin de paréntesis: [Smartparents](https://github.com/Fuco1/smartparens).
	e) Vterm para consola.
	
- Distintos formatos (también imprescindible): Epub, PDF, Markdown, ...

- Manejo de directorios y ficheros:

a) Poder filtrar cómodamente con [dired-narrow](https://github.com/Fuco1/dired-hacks).
b) Uso de directorios en dired.
c) Flexibilidad de ordenación con [dired-quick-sort](https://github.com/emacsmirror/dired-quick-sort).

- Manejo de texto: Diccionarios, ...

- Presentación. Modo para configurar el powerline, el Tema de [modus-themes](https://protesilaos.com/emacs/modus-themes) que es lo mejor que se ha inventado, más legible, iconos, ...

- Evil, para manejo de teclas Vim para Emacs.

- Correo, mucho más facil de configurar con [notmuch](https://notmuchmail.org/).

- Soporte de org: [org-aggregate](https://github.com/emacsmirror/orgtbl-aggregate) que permite crear tablas resumen, añadir tablas de contenidos con [toc-org](https://github.com/snosov1/toc-org), ...

- Soporte multimedia.

- Redes sociales, como [mastodon](https://codeberg.org/martianh/mastodon.el).

En total, tampoco ha sido tanto tiempo, y creo que puede ser un buen punto de partida. Lo usaré para mi tutorial, evidentemente.
