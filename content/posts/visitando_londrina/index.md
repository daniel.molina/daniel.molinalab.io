+++
title = "Visitando Londrina"
date = 2023-11-29
[taxonomies]
tags = ["research", "teaching"]
+++

Entre Noviembre y Diciembre he viajado a Londrina en Brasil. En particular, he visitado la [Universidade Estadual de Londrina](https://portal.uel.br/home/).

He realizado el viaje acompañando a mi pareja, que tenía comprometido dar un curso en un programa de máster de ahí, mientras que en mi caso he sido invitado 
por personal del Departamento de Computacao. En mi caso, he participado:

- Visitando el [Departamento de Computaçao de Londrina](https://departamentos.uel.br/computacao/), gracias a la invitación de Daniel dos Santos Kaster, que está siendo muy amable conmigo. También he dado una charla en su Dpto sobre mis trabajos y los de mi grupo, y hemos empezado contacto para posibles colaboraciones.

![Charla en el Departamento](yo_charla1.jpg)

- Por otro lado, también he dado una charla de divulgación para estudiantes en general:

![Cartel de la Charla de Divulgación](cartel_charla.jpg)

Que estuvo muy bien:

![Charla en Londrina](yo_charla2.jpg)

Con una presentación muy chula:

[![Portada de la charla](portada_transparencias.png)](https://zenodo.org/doi/10.5281/zenodo.10200793)

Que he puesto libremente [disponible en zenodo](https://zenodo.org/doi/10.5281/zenodo.10200793), que por cierto, es todo un descubrimiento para almacenar contenido de interés como transparencias. También está en mi sección de [Charlas](/talks).

