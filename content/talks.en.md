+++
title = "Talks and Courses Material"
author = "Daniel Molina"
date = 2023-03-31
draft = false
[taxonomies]
tags = ["teaching"]
+++


# Talks In English

In this page I show teaching or divulgation material, it is a compilation of the different repositories that I own. Before using any of them, please consult the corresponding license.

Here I only show the material translated in English, for material in Spanish you can check the website <a href="/talks">in Spanish.</a>

# Talks

## Outreach talks

### About me

{{rimage(path="/images/charla_aboutme.png", scale="50%", alt="Cartel de la charla sobre mí")}}

Presentation for search collaborations, explaining my recent research works.

- [Slides](/talks/about_me.pdf)

## About Free Software

Short course about Machine Learning (inside an Erasmus+ program)

- [Slides](https://github.com/dmolina/ml_python_course_short)

Talk in JuliaCon 2022 about my teaching experience with Julia

- [Video](https://www.youtube.com/watch?v=CwDntVE3SHU)

Talk in JuliaCon 2021 about my package DaemonMode, that allow us to run faster julia for scripts.

- [Video](https://www.youtube.com/watch?v=wCkuXAdE8E0)
- [Package](https://github.com/dmolina/DaemonMode.jl)
- [Light client](https://github.com/dmolina/juliaclient_nim)

Talk in PlutoCon 2021

- [Slides](https://github.com/dmolina/PlutoCon2021-demos)

- [Video](https://www.youtube.com/watch?v=NrIxgnFXslg)
