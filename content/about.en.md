+++
title = "About me"
author = "Daniel Molina"
date = 2020-07-01
lang = "en"
draft = false
[taxonomies]
tags = ["personal"]
+++

{{fig(fname="/images/me_small.jpg", caption="Which is this crazy guy?")}}

I am a senior lecturer at the University of Granada, specialized in Artificial
Intelligence. As many people, I have several roles:

-   Since a researcher side, I have an index-h of 23, with more than 24 papers
    (more than 90% in Q1), with more than 7000 citations. In the main menu, you can
    see my publications (in many cases with the PDF freely available, ask me if you
    need any other). Also, I was until this year the Chair on the [IEEE CIS Task Force on
    Large-Scale Global Optimization](http://tflsgo.org/), because it is a type of optimization I am
    particularly interested (I was winner in two international competitions, at
    2010 and more recently at 2018). I am member of the _Andalucian Research
    Institute in Data Science and Computational Intelligence_ [DaSCI](https://dasci.es/)

-   As a teacher, now I am teaching at the [University of
    Granada](https://www.ugr.es), in the [Technical School of Technology Engineering](https://etsiit.ugr.es/), but for
    more than 10 years I was teaching at the University of Cádiz, where I left a
    part of my hearts (people were more my friends than my colleagues).

-   As a computer science that love programming, I am interested in programming
    in different languages (like C++, Java, ....), and I particularly love Python,
    and another interesting growing programming languages (like Rust or Julia). I define
    myself also as a Linux user, this is the only OS in my computers for more than
    15 years. Also, I am a believer in [Free Software](https://www.gnu.org/philosophy/free-sw.html) (actually, I was the
    secretary of Free Software Office at the University of Cadiz).

-   As a real-world person, I love reading books, watching films, and spend my
    free time with my girlfriend Amalia :heart:.
