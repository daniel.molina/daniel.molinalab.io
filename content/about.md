+++
title = "Sobre mi"
author = "Daniel Molina"
date = 2020-07-01
draft = false
[taxonomies]
tags = ["personal"]
+++

{{fig(fname="/images/me_small.jpg", caption="¿Quien es ese loco?")}}

Soy Titular de la Universidad de Granada, especializado en Inteligencia
Artificial y Metaheurísticas. Como mucha gente, tengo varios roles:

-   Como investigador, tengo un índice h de 23, con más de 24 trabajos (más del
    90% en Q1), y más de 7000 citas. En la portada, puede consultar mis
    publicaciones en ORCID y en Google Schoolar, y pondré un enlace con mis
    publicaciones (en muchos casos con el PDF disponible, escribame si
    necesitase alguno que no apareciese). Además, he sido hasta 2019 el Chair
    del [IEEE CIS Task Force on Large-Scale Global
    Optimization](http://tflsgo.org/), ya que es un tipo de optimización en el
    que tengo especial interés (he ganado dos competiciones internacionales
    sobre LSGO, en 2010 y más recientemente en 2018). Formo parte del Instituto
    Andaluz Interuniversitario en _Data Science and Computational Intelligence_
    [DaSCI](https://dasci.es/).

-   Como profesor, enseño en la [Universidad de Granada](https://www.ugr.es), en
    la [Escuela Técnica Superior de Ingeniería](https://etsiit.ugr.es/), y
    durante más de 10 años fui profesor de la Universidad de Cádiz, en donde
    dejé parte de mi corazón (esa gente fueron más amigos que colegas).

-   Como informático adoro programar, en distintos lenguajes (C++, Java, ...) y
    en particular tengo predilección por Python y otros lenguajes prometedores
    (como Rust o Julia).
    
-   Me defino a mí mismo como Linuxero, Linux es el único Sistema Operativo de
    mi ordenador personal por más de 15 años. También soy un feliz usuario de
    Emacs (y anteriormente de Vim, adoro ambos). Además, defensor del [Software
    Libre](https://www.gnu.org/philosophy/free-sw.html) (llegué a ser durante
    años Secretario de la Oficina de Software Libre, OSLUCA, de la Universidad
    de Cádiz).

-   Como persona del mundo real, adoro leer libros de todo tipo, ver películas y
    pasar la mayor parte de mi tiempo libre con mi novia Amalia :heart:.
