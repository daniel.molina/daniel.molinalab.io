+++
title = "Charlas y Material Docente"
author = "Daniel Molina"
date = 2023-03-31
draft = false
[taxonomies]
tags = ["teaching"]
+++

En esta entrada muestro material docente o de divulgación, es una recopilación de los distintos repositorios que poseo. Antes de utilizar cualquiera de ellos, consulte su licencia correspondiente.

# Material Docente oficial

A continuación muestro material más oficial

- [Material de la asignatura de Metaheurísticas](https://sci2s.ugr.es/node/124). Se muestra tanto el material de teoría como de prácticas para la asignatura de Metaheurísticas del Grado de Ingeniería Informática de la Universidad de Granada. Dentro del curso se usa la plataforma Moodle, pero este material se replica en la web de nuestro grupo de investigación.

Material sobre asignatura de _Preprocesamiento_ en el Máster de Ciencias de Datos:

- [Transparencias](https://master.danimolina.net/)

- [Código fuente](https://codeberg.org/dmolina/master_preprocesamiento)


# Charlas

## Divulgativas sobre Inteligencia Artificial

### Inteligencia artificial en el siglo XXI: Qué nos puede ofrecer, y de qué tenemos que tener cuidado

{{rimage(path="/images/charla_IAsigloXXI.png", scale="50%", alt="Cartel de charla sobre la IA en el Siglo XXI")}}

- [Transparencias en Zenodo](https://zenodo.org/doi/10.5281/zenodo.10200793).

### La Naturaleza, inspiradora de Inteligencia Artificial

{{rimage(path="/images/charla_lanoche.png", scale="50%", alt="Cartel de charla sobre la Noche")}}

- [Transparencias en Zenodo](https://zenodo.org/records/10049648).
- [Software Demo sobre Algoritmo Bio-Inspirado](https://codeberg.org/dmolina/innovaMH)
- [Software Demo de Identificación de Animal](https://codeberg.org/dmolina/demo_animal)

<!--
### Inteligencia artificial en el Siglo XXI: Qué nos puede ofrecer, y de qué tenemos que tener cuidado

{{rimage(path="/images/charla_brasil.png", scale="50%", alt="Cartel de charla sobre IA en el Siglo XXI")}}

- [Transparencias en Zenodo]()
-->

### About me

{{rimage(path="/images/charla_aboutme.png", scale="50%", alt="Cartel de la charla sobre mí")}}

- [Transparencias](/talks/about_me.pdf)

## Sobre Software Libre

Charla sobre Mastodon (colaboración con la Oficina de Software Libre, en 2023)

{{rimage(path="/images/charla_mastodon.png", scale="50%", alt="Cartel de charla sobre Mastodón")}}

- [Transparencias](https://sobremastodon.danimolina.net/)

Curso corto de Machine Learning de un plan Erasmus+ (Inglés)

- [Transparencias](https://github.com/dmolina/ml_python_course_short)

Taller en el PyConES del 2022

- [Transparencias](https://codeberg.org/dmolina/pycones_2022_eficiencia)

Charla en el JuliaCon 2022 (Inglés) sobre mi experiencia docente usando Julia

- [Vídeo](https://www.youtube.com/watch?v=CwDntVE3SHU)

Charla en el JuliaCon 2021 (Inglés) sobre mi utilidad DaemonMode, que permite ejecutar rápidamente Julia para scripts.

- [Vídeo](https://www.youtube.com/watch?v=wCkuXAdE8E0)
- [Package](https://github.com/dmolina/DaemonMode.jl)
- [Cliente ligero (en Nim)](https://github.com/dmolina/juliaclient_nim)

Presentación oficial en el PlutoCon 2021 (Inglés)

- [Transparencias](https://github.com/dmolina/PlutoCon2021-demos)

- [Vídeo](https://www.youtube.com/watch?v=NrIxgnFXslg)

Charla sobre FastAPI (en Instituto de FP invitado, y luego en la ETSIIT de la UGR en 2023)

{{rimage(path="/images/charla_fastapi.png", scale="50%", alt="Cartel de la clase de FastAPI")}}

- [Transparencias](https://github.com/dmolina/charla_fastapi)

Taller sobre Pandoc (colaboración con la Oficina de Software Libre) en 2022

{{rimage(path="/images/charla_pandoc.png", scale="50%", alt="Cartel del taller de pandoc")}}

- [Transparencias](https://github.com/dmolina/taller_pandoc)
