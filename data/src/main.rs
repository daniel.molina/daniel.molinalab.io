use biblatex::Bibliography;
use std::env;
use std::fs;

fn main() -> std::io::Result<()> {
    let fname = "all.bib";
    let contents = fs::read_to_string(fname).expect("No encontrado el fichero");

    let all = Bibliography::parse(&contents).unwrap();

    for item in all {
        let content_item = item.to_bibtex_string().unwrap();
        println!("Salida: {}", content_item);
        let current = env::current_dir().unwrap();
        let parent = current.parent().unwrap();
        let foutput = parent.join("static").join("bib").join(item.key + ".bib");

        if fs::metadata(&foutput).is_err() {
            fs::write(foutput, content_item).unwrap();
        }
    }
    return Ok(());
}
